<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Person.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Admin.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Student.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Teacher.php';

if($_GET['entry'] ==='second'){
    try{
        $sql = "SELECT * FROM members;";
        $answer  = $db->query($sql);
        $members = $answer->fetchAll();

    }catch(Exception $bug){
        die('Problem with getting data ' .$bug->getMessage());
    }
};
$objects = [];
if($members){
    foreach($members as $member){
        switch($member['role_member']){
            case 'student':
                $objects[] = new Student(
                    $member['full_name'],
                    $member['phone'],
                    $member['email'],
                    $member['role_member'],
                    $member['averange_mark']
                );
            break;
            case 'teacher':
                $objects[] = new Teacher(
                    $member['full_name'],
                    $member['phone'],
                    $member['email'],
                    $member['role_member'],
                    $member['subject_'] 
                );
            break;
            case 'admin':
                $objects[] = new Admin(
                    $member['full_name'],
                    $member['phone'],
                    $member['email'],
                    $member['role_member'],
                    $member['working_day'] 
                );
            break;

        }
    }
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <meta name="Description" content="My first test Database">
    <title>DB</title>
</head>
<body>
    <a href="/database/create_db.php"><?php if(!$_GET['entry']) : echo'Создать Базу Данных'; endif;?></a>
    <?php foreach($objects as $key => $object): ?>
        <p><?=++$key.') '.$object->getVisitCard(); ?></p>
        <br/>
    <?php endforeach; ?>
</body>
</html>