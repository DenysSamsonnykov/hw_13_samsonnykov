<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';

try{
    $sql = "CREATE TABLE members(
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        full_name VARCHAR(255),
        phone VARCHAR(255),
        email VARCHAR(255),
        role_member VARCHAR(255),
        averange_mark FLOAT,
        subject_ VARCHAR(255),
        working_day VARCHAR(255))
        DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;";
    $db->exec($sql);
    }catch(Exception $error){
        die('Error creating members table' . $error->getMessage());
    }
    echo 'db create!'
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <meta name="Description" content="My first test Database">
    <title>DB</title>
</head>
<body>
    <br/>
    <a href="/database/seeder_db.php">Заполнить Базу Данных</a>
</body>
</html>