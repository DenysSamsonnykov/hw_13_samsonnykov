<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';

$allMembers = [
    [
        'full_name' => 'Elena Gol',
        'phone' => '+380987654556',
        'email' => 'Elena@gmail.com',
        'role_member' => 'student',
        'averange_mark' => 10.5
        
    ],
    [
        'full_name' => 'Ojen Astro',
        'phone' => '+380934703816',
        'email' => 'crud34@gmail.com',
        'role_member' => 'student',
        'averange_mark' => 11.8
    
    ],
    [
        'full_name' => 'Anatoliy Egorov',
        'phone' => '+380636789921',
        'email' => 'EA@gmail.com',
        'role_member' => 'student',
        'averange_mark' => 5.5
       
    ],
    [
        'full_name' => 'Ivan Ehoo',
        'phone' => '+380954445567',
        'email' => 'Ehoo@mail.ru',
        'role_member' => 'teacher',
        'subject_' => 'IT'

    ],
    [
        'full_name' => 'Victor Juk',
        'phone' => '+380956899090',
        'email' => 'Juk@gmail.com',
        'role_member' => 'teacher',
        'subject_' => 'Mathematics'
    ],
    [
        'full_name' => 'Levan Gro',
        'phone' => '+380953335767',
        'email' => 'Goro@mail.ru',
        'role_member' => 'teacher',
        'subject_' => 'English'
    ],
    [
        'full_name' => 'Lulo Baltimor',
        'phone' => '+380637733789',
        'email' => 'luloo@mail.com',
        'role_member' => 'admin',
        'working_day' => 'Wednesday'
    ],
    [
        'full_name' => 'Lilo Macvil',
        'phone' => '+380639080799',
        'email' => 'liloS@gmail.com',
        'role_member' => 'admin',
        'working_day' => 'Saturday'
    ],
    [
        'full_name' => 'Stich Jobs',
        'phone' => '+380678586900',
        'email' => 'qwrty@mail.com',
        'role_member' => 'admin',
        'working_day' => 'Thursday'
    ]
];


foreach($allMembers as $member){
    $sql = "INSERT INTO members SET 
    full_name='{$member['full_name']}',
    phone='{$member['phone']}',
    email='{$member['email']}',
    role_member='{$member['role_member']}'";
    switch($member['role_member']){
        case 'student':
            try{
                $sql .=",
                averange_mark = {$member['averange_mark']}";
                $db->exec($sql);
            }catch(Exception $error){
                die('Error inserting test data'. $error->getMessage());
        };
        break;
        case 'teacher' :
            try{
                $sql .= ",
                subject_='{$member['subject_']}'";
                $db->exec($sql);
            }catch(Exception $error){
                die('Error inserting test data'. $error->getMessage());
        };
        break; 
        case 'admin' :
            try{
                $sql .= ",
                working_day='{$member['working_day']}'";
                $db->exec($sql);
            }catch(Exception $error){
                die('Error inserting test data'. $error->getMessage());
        };
        break;

    }
}
echo 'Data added successfully';   
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <meta name="Description" content="My first test Database">
    <title>DB</title>
</head>
<body>
    <br/>
    <a href="/?entry=second">Глянуть результат</a>
</body>
</html>