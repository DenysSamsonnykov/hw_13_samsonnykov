<?php

class Person {
    public $fullName;
    public $phone;
    public $email;
    public $roleMember;

    public function __construct($fullName, $phone, $email, $roleMember) {
        $this->fullName = $fullName;
        $this->phone = $phone;
        $this->email = $email;
        $this->roleMember = $roleMember;
    }

    public function getVisitCard(){
        return 'Full name: '. $this->fullName .
        ', Phone: ' . $this->phone . 
        ', Email: ' . $this->email . 
        ', Role: ' . $this->roleMember;
    }
}


?>