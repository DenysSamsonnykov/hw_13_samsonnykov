<?php

class Teacher extends Person {
    public $subject;
    public function __construct($fullName, $phone, $email, $roleMember, $subject){
        parent::__construct($fullName, $phone, $email, $roleMember);
        $this->subject = $subject;
    }

    public function getVisitCard(){
       return parent::getVisitCard(). ', Subject: ' . $this->subject;
    }

}

?>