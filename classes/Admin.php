<?php

class Admin extends Person {
    public $workingDay;
    public function __construct($fullName, $phone, $email, $roleMember, $workingDay){
        parent::__construct($fullName, $phone, $email, $roleMember);
        $this->workingDay = $workingDay;
    }

    public function getVisitCard(){
       return parent::getVisitCard(). ', Working day: ' . $this->workingDay;
    }

}

?>