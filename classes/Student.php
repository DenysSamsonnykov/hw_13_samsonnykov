<?php

class Student extends Person {
    public $averangeMark;
    public function __construct($fullName, $phone, $email, $roleMember, $averangeMark){
        parent::__construct($fullName, $phone, $email, $roleMember);
        $this->averangeMark = $averangeMark;
    }

    public function getVisitCard(){
       return parent::getVisitCard(). ', Averange mark: ' . $this->averangeMark;
    }

}

?>